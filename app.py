from flask import Flask , render_template
app = Flask(__name__)

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/overview')
def overview():
    return render_template('overview.html')

@app.route('/analysis')
def analysis():
    return render_template('analysis.html')

@app.route('/classification')
def classification():
    return render_template('classification.html')


if __name__ == "__main__":
	app.run(debug=True)
